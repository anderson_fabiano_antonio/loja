
import 'package:flutter/material.dart';
import 'package:loja/screens/login_screen.dart';

class EmptyLogIn extends StatelessWidget {

  final String texto;
  final IconData icone;
  EmptyLogIn(this.texto,this.icone);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            icone,
            size: 80,
            color: Theme.of(context).primaryColor,
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            "$texto",
            style: TextStyle(
                fontSize: 20,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 16,
          ),
          RaisedButton(
            child: Text(
              "Entrar",
              style: TextStyle(fontSize: 18),
            ),
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
          )
        ],
      ),
    );
  }
}
