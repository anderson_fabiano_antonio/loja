import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loja/models/user_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_correios/flutter_correios.dart';
import 'package:flutter_correios/model/resultado_cep.dart';
import 'package:brasil_fields/brasil_fields.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _enderecoController = TextEditingController();
  final _cepController = TextEditingController();
  final _numController = TextEditingController();
  final _complController = TextEditingController();
  final _cpfController = TextEditingController();
  final _telefoneController = TextEditingController();
  final _dataNascimentoController = TextEditingController();

  String _emailValido(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Digite um E-mail valido!';
    else
      return null;
  }

  final _formKey = GlobalKey<FormState>();
  final _scaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffold,
        appBar: AppBar(
          title: Text("Criar Conta"),
          centerTitle: true,
          actions: <Widget>[],
        ),
        body:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          if (model.isLoading)
            return Center(
              child: CircularProgressIndicator(),
            );
          return Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: <Widget>[
                SizedBox(
                  height: 2,
                ),
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    labelText: "Nome Completo",
                  ),
                  maxLength: 50,
                  validator: (text) {
                    if (text.length < 5) return "Nome Invalido";
                    //_validarNome(text);
                  },
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: _cpfController,
                          keyboardType: TextInputType.numberWithOptions(),
                          decoration: InputDecoration(
                            labelText: "CPF",
                          ),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            CpfInputFormatter(),
                          ],
                          maxLength: 1411,
                          validator: (text) {
                            //_validarNome(text);
                          },
                        ),
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                        child: TextFormField(
                          controller: _dataNascimentoController,
//                          keyboardType: TextInputType.numberWithOptions(),
                          decoration: InputDecoration(
                            labelText: "Data Nascimento",
                          ),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            DataInputFormatter(),
                          ],
                          maxLength: 1411,
                          validator: (text) {
                            //_validarNome(text);
                          },
                        ),
                      ),

                    ]),
                SizedBox(
                  height: 2,
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    labelText: "E-mail",
                  ),
                  maxLength: 100,
                  keyboardType: TextInputType.emailAddress,
                  validator: (text) {
                    return _emailValido(text);
                  },
                ),
                SizedBox(
                  height: 2,
                ),
                TextFormField(
                  controller: _passController,
                  decoration: InputDecoration(
                    labelText: "Senha",
                  ),
                  obscureText: true,
                  maxLength: 20,
                  validator: (text) {
                    if (text.isEmpty || text.length < 6)
                      return "Senha Inválida";
                  },
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: _cepController,
                          decoration: InputDecoration(
                            labelText: "CEP",
                          ),
                          keyboardType: TextInputType.number,
                          maxLength: 10,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            CepInputFormatter(),
                          ],
                          onFieldSubmitted: (text) {
                            buscaCep(text);
                          },
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: TextFormField(
                          controller: _telefoneController,
                          decoration: InputDecoration(
                            labelText: "Telefone",
                          ),
                          keyboardType: TextInputType.number,
                          maxLength: 15,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            
                            TelefoneInputFormatter(),
                          ],
                          onFieldSubmitted: (text) {
                            buscaCep(text);
                          },
                        ),
                      ),
                    ]),
                SizedBox(
                  height: 2,
                ),
                TextFormField(
                  controller: _enderecoController,
                  decoration: InputDecoration(
                    labelText: "Endereço",
                  ),
                  maxLength: 200,
                  validator: (text) {
                    if (text.isEmpty || text.length < 5)
                      return "Endereço Invalido";
                  },
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: _numController,
                        decoration: InputDecoration(
                          labelText: "Nº",
                        ),
                        maxLength: 200,
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: _complController,
                        decoration: InputDecoration(
                          labelText: "Complemento",
                        ),
                        maxLength: 200,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2,
                ),
                SizedBox(
                  height: 44,
                  child: RaisedButton(
                    child: Text(
                      "Criar Conta",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        Map<String, dynamic> userData = {
                          "nome": _nameController.text,
                          "email": _emailController.text,
                          "endereco": _enderecoController.text,
                          "numero": _numController.text,
                          "cep": _cepController.text,
                          "complemento": _complController.text,
                          "cpf": _cpfController.text,
                          "telefone": _telefoneController.text,
                          "datanascimento": _dataNascimentoController.text,

                        };
                        model.signUp(
                            userData: userData,
                            pass: _emailController.text,
                            onSuccess: _onSuccess,
                            onFail: _onFail);
                      }
                    },
                  ),
                ),
              ],
            ),
          );
        }));
  }

  void _onSuccess() {
    _scaffold.currentState.showSnackBar(
      SnackBar(
        content: Text("Usuario Criado com Sucesso!!"),
        backgroundColor: Theme.of(context).primaryColor,
        duration: Duration(seconds: 2),
      ),
    );
    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pop();
    });
  }

  void _onFail() {
    _scaffold.currentState.showSnackBar(
      SnackBar(
        content: Text("Falha ao Criar Usuario!!"),
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 2),
      ),
    );
  }

  void buscaCep(cep) async {
    final FlutterCorreios fc = FlutterCorreios();
    ResultadoCEP resultado =
        await fc.consultarCEP(cep: cep.replaceAll('.', ''));
    _enderecoController.text = resultado.logradouro;
  }
}
