import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja/models/user_model.dart';
import 'package:loja/tiles/order_tile.dart';
import 'package:loja/widgets/empty_login.dart';

class OrderTabe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    if(UserModel.of(context).isLoggedIn()){
      String uid = UserModel.of(context).firebaseUser.uid;
      return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("users").document(uid).collection("orders").getDocuments(),
        builder: (c, s) {
          if(!s.hasData)
            return Center(child: CircularProgressIndicator(),);
          else
          {
            return ListView(
              children: s.data.documents.map((doc) => OrderTile(doc.documentID)).toList().reversed.toList(),
              );
          }
        },
      );
    } else {
      return EmptyLogIn("Faça Login para acompanhar!", Icons.format_list_bulleted);
    }

  }
}